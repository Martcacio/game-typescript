import { createStore } from "redux";
//import { devToolsEnhancer } from "redux-devtools-extension";

import rootReducer from "../reducers/rootReducer";

import { State } from "../reducers/index";
import { Actions } from "../actions/actions";

const store = createStore<{ status: State }, Actions, null, null>(rootReducer);

export default store;
