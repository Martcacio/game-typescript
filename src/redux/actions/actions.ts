import { Types } from "../../atoms/Button";

export interface ISetGreetingsActions {
  readonly type: "SET_GREETINGS";
  payload?: Types;
}
export type Actions = ISetGreetingsActions;
