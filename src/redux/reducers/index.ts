import { Actions } from "../actions/actions";
import { Types } from "../../atoms/Button/index";

export type State = {
  status: Types;
};

const initialState: State = {
  status: Types.Hello,
};

const Reducer = (state: State = initialState, action: Actions): State => {
  switch (action.type) {
    case "SET_GREETINGS":
      return {
        ...state,
        status: action.payload ? action.payload : Types.Hello,
      };
    default:
      return state;
  }
};

export default Reducer;
