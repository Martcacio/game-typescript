import Reducer from "../reducers";


const rootReducer ={state:any, action:any} => Reducer(state, action);


export type AppState = ReturnType<typeof rootReducer>;

export default rootReducer;
