import ReactDOM from "react-dom";
import "./index.css";
import Board from "./molecules/Board/index";

ReactDOM.render(<Board />, document.getElementById("root"));
