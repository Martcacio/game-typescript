import React, { Dispatch } from "react";
import { useDispatch } from "react-redux";
import { Actions } from "../../redux/actions/actions";

export enum Types {
  Hello = "Hello",
  Bye = "Bye",
}

const Button: React.FC = () => {
  const greetingsDispatch = useDispatch<Dispatch<Actions>>();
  const handleSetGreetings = () => {
    greetingsDispatch({ type: "SET_GREETINGS", payload: Types.Hello });
  };

  return (
    <button
      onClick={() => handleSetGreetings()}
      type="button"
      className="restart"
    ></button>
  );
};

export default Button;
