import React from "react";
import "../../index.css";
import * as Types from "./SquareTypes";

const Square: React.FC<Types.SquareTypes> = ({
  value,
  handleOnClick,
}: Types.SquareTypes) => (
  <button className="square" onClick={handleOnClick}>
    {value}
  </button>
);

export default Square;
