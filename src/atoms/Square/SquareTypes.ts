export type SquareValue = "O" | "X";

export interface SquareTypes {
  value?: SquareValue;
  handleOnClick: React.MouseEventHandler<HTMLButtonElement>;
}
