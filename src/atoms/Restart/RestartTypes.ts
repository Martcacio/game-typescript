export type RestartValue = "Restart";

export interface RestartTypes {
  value: RestartValue;
  handleOnClick: React.MouseEventHandler<HTMLButtonElement>;
}
