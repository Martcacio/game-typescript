import React from "react";
import "../../index.css";
import * as Types from "./RestartTypes";

const Restart: React.FC<Types.RestartTypes> = ({
  value,
  handleOnClick,
}: Types.RestartTypes) => (
  <button className="restart" onClick={handleOnClick}>
    {value}
  </button>
);

export default Restart;
